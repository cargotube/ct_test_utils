-module(ct_test_utils).

-export([gen_global_id/0, to_existing_atom/1, to_existing_atom/2,
         wait_for_process_to_die/2, wait_for_true/2, process_running/1, meck_new/1, meck_done/1]).

gen_global_id() ->
    rand:uniform(9007199254740993) - 1.

to_existing_atom(Value) ->
    to_existing_atom(Value, {error, not_possible}).

to_existing_atom(Atom, _Default) when is_atom(Atom) ->
    Atom;
to_existing_atom(Bin, Default) when is_binary(Bin) ->
    try
      binary_to_existing_atom(Bin, utf8)
    catch
      _:_ ->
          Default
    end;
to_existing_atom(List, Default) when is_list(List) ->
    to_existing_atom(list_to_binary(List), Default).

process_running(Pid) ->
    case process_info(Pid) of
      undefined ->
          false;
      _ ->
          true
    end.

wait_for_process_to_die(Pid, Iterations) ->
    PidWait =
        fun () ->
                not process_running(Pid)
        end,
    case wait_for_true(PidWait, Iterations) of
      ok ->
          ok;
      _ ->
          still_alive
    end.

wait_for_true(_Fun, 0) ->
    failed;
wait_for_true(Fun, Iterations) ->
    case Fun() of
      true ->
          ok;
      _ ->
          timer:sleep(10),
          wait_for_true(Fun, Iterations - 1)
    end.

meck_new([]) ->
    ok;
meck_new([H | T]) ->
    meck:new(H),
    meck_new(T).

meck_done([]) ->
    ok;
meck_done([H | T]) ->
    true =
        case meck:validate(H) of
          true ->
              true;
          _ ->
              io:format("invalid call to mock of ~p~n", [H]),
              false
        end,
    ok = meck:unload(H),
    meck_done(T).
